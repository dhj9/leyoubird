import React from 'react';
import {render} from 'react-dom';
import {Router, Route, hashHistory, IndexRoute} from 'react-router';

import Header from './components/header.js';
import MainBody from './components/mainBody.js';
import Footer from './components/footer.js';

import HeaderScrollbar from './components/headerScrollbar.js';
import MainList from './components/mainList.js';

import D from './dispatch.js';

require('./less/leyoubird.less');

let Leyoubird = React.createClass({
    render: function () {
        return (
            <div data-role="page">
                <Header />
                <MainBody>
                    <HeaderScrollbar />
                    <MainList linkUrl="/api/travel" category="recommend" />
                </MainBody>
                <Footer />
            </div>
        );
    }
});

let route = (
    <Router history={hashHistory}>
        <Route path="/">
            <IndexRoute component={Leyoubird} />
            <Route path="recommend" component={Leyoubird} />
        </Route>
    </Router>
);

D.on('machi', 1, 2);
render(route, document.getElementById('leyoubird'));

