import React from 'react';

let Footer = React.createClass({
    render: function () {
        return (
            <footer data-role="footer" className="le-main-footer">
                <div className="le-footer-btn-list">
                    <a href="" className="le-footer-btn focus">
                        <div className="le-footer-btn-icon">&#xf015c;</div>
                        <div className="le-footer-btn-content">热门推荐</div>
                    </a>
                    <a href="shuttleService.html" className="le-footer-btn">
                        <div className="le-footer-btn-icon">&#xe651;</div>
                        <div className="le-footer-btn-content">接送服务</div>
                    </a>
                    <a href="customTrip.html" className="le-footer-btn">
                        <div className="le-footer-btn-icon">&#xf0126;</div>
                        <div className="le-footer-btn-content">定制旅行</div>
                    </a>
                    <a href="deepenExperience.html" className="le-footer-btn">
                        <div className="le-footer-btn-icon">&#xe64f;</div>
                        <div className="le-footer-btn-content">深度体验</div>
                    </a>
                    <a href="" className="le-footer-btn">
                        <div className="le-footer-btn-icon">&#xf01d8;</div>
                        <div className="le-footer-btn-content">我的旅行</div>
                    </a>
                </div>
            </footer>
        );
    }
});

export default Footer;