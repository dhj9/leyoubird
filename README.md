# Leyoubird 乐游鸟

------

乐游鸟旅行（Leyoubird），专业的泰国旅行服务提供者。机票、酒店、接送机、地陪、婚纱等服务应有尽有。

## 关于本项目

该项目是乐游鸟旅行线上微信推广页的改版项目，用于探索和实践新的技术框架。
您可以访问[线上版本](http://www.leyoubird.com)来查看它的全部功能。

## 你需要？

* node, npm
* webpack

## 快速查看

1. 任意控制台工具，进入项目跟目录
2. `npm install`
3. `npm run dev`
4. 打开浏览器，输入`http://127.0.0.1:8080/`即可查看效果

## 核心技术点

* webpack
* React

## License

MIT